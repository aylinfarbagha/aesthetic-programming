Link to my program: https://aylinfarbagha.gitlab.io/aesthetic-programming/miniX1/

I've made a sketch that's supposed to show the universe with it's bright stars and pretty coloured planets. The stars are rotating to create an experience of movement.

I had no idea what I was doing, and most of my code was copied - the code for the stars I found by googling "how to make stars in p5.js". I decided to make numerous stars and label them //star1, //star2, //star3 etc., and under each of them I made smaller stars and sorted them by size. This made it easier for me to know which star was what, by sorting them by size, and by knowing that the smaller stars were always connected to one of the larger ones. I also made some of the stars transparent in order for them to blend in to create some background/foreground, which I placed away for themselves under //star5 (transparent).

The coding process for me was very different to reading and writing, as I already know how to do those things due to my knowledge of language. But with this, it is like learning a whole new language, with its own words, meanings and grammatical setups.

Reflecting on coding after reading Vee's text on literacy, I now see it as a very important tool that is necessary in terms of gaining knowledge about the world we live in now, as well as understanding the functions behind its systems.
