
function setup() {
  //put setup code here
  createCanvas(1550, 719);
    print("helloworld");
//for later use
// Show mouse coordinates on console
print(mouseX,mouseY);
}

function draw() {
    print(mouseX,mouseY);
  //setup
background(0,0,0)

//stars
fill(255,255,0);

//star1
push();
translate(width * 0.2, height * 0.5);
rotate(frameCount / 200.0);
star(0, 0, 3, 80, 9);
//circle 0, circle 0, size of star, size of circumference, number of lines
pop();

//star2
push();
translate(width * 0.35, height * 0.3);
rotate(frameCount / 200.0);
star(0, 0, 2, 120, 7);
pop();

push();
translate(width * 0.3, height * 0.2);
rotate(frameCount / 200.0);
star(0, 0, 2, 80, 7);
pop();

push();
translate(width * 0.3, height * 0.45);
rotate(frameCount / 200.0);
star(0, 0, 2, 30, 7);
pop();

push();
translate(width * 0.4, height * 0.4);
rotate(frameCount / 200.0);
star(0, 0, 2, 20, 7);
pop();

push();
translate(width * 0.35, height * 0.55);
rotate(frameCount / 200.0);
star(0, 0, 2, 15, 7);
pop();


//star3
push();
translate(width * 0.5, height * 0.6);
rotate(frameCount / 200.0);
star(0, 0, 2, 150, 7);
pop();

push();
translate(width * 0.6, height * 0.65);
rotate(frameCount / 200.0);
star(0, 0, 2, 40, 7);
pop();

push();
translate(width * 0.65, height * 0.4);
rotate(frameCount / 200.0);
star(0, 0, 2, 35, 7);
pop();

push();
translate(width * 0.5, height * 0.4);
rotate(frameCount / 200.0);
star(0, 0, 2, 30, 7);
pop();

//star4
fill(255,255,0,150);
push();
translate(width * 0.8, height * 0.25);
rotate(frameCount / 200.0);
star(0, 0, 4, 250, 9);
pop();

push();
translate(width * 0.72, height * 0.15);
rotate(frameCount / 200.0);
star(0, 0, 2, 40, 7);
pop();

push();
translate(width * 0.7, height * 0.3);
rotate(frameCount / 200.0);
star(0, 0, 2, 30, 7);
pop();

push();
translate(width * 0.6, height * 0.2);
rotate(frameCount / 200.0);
star(0, 0, 2, 25, 7);
pop();

//star5 (transparent)
fill(255,255,0,100);
push();
translate(width * 0.6, height * 0.5);
rotate(frameCount / 200.0);
star(0, 0, 4, 200, 9);
pop();

fill(255,255,0,70);
push();
translate(width * 0.35, height * 0.5);
rotate(frameCount / 200.0);
star(0, 0, 4, 190);
pop();

fill(255,255,0,70);
push();
translate(width * 0.2, height * 0.3);
rotate(frameCount / 200.0);
star(0, 0, 4, 250, 9);
pop();
//planets
  //planet1
  fill(2558,155,155);
  circle(500,500,80);

  fill(50,0,255);
  circle(744,183,50);

  fill(150,150,255);
  circle(182,126,50);

  fill(60,10,90);
  circle(1093,318,90);

  fill(90,0,190);
  circle(1165, 393.800000190734860,60);

}
//enablestar
  function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }
