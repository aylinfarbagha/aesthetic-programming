Link to revised miniX3: https://aylinfarbagha.gitlab.io/aesthetic-programming/miniX6/

**Which MiniX do you plan to rework?**

I plan to revisit my MiniX3 where I had to make a throbber. Here we learned the notion of loops and temporality in coding. In my feedback I was told that I needed to incorporate the theories from class more in my readmes, specifically with regards to the execution time (how long it takes for the computer to run a code) vs the timing of execution (how draw() is called at a specific rate, i.e. the rythm). I was told that a good method could be to take a few quotes from the texts that I've read and try to apply it when I created my code, so that I would have theoretical grounds to back up and further describe my miniX. I am choosing this MiniX as an exercise for myself, as I feel quite "weak" in this specific theme, and that I can do better with this MiniX, as least with my readme, by reflecting more in depth from the text's I've read, specifically in the Aesthetic Programming book and the text by Hans Lammerant: _How humans and machines negoiate the experience of time_. Furthermore, to take my feedback into practice, I will be taking a quote from a text and using it as my baseline to add a new sequence to my code.

**What have you changed and why?**

I was reading the Lammerant text again, and found an interesting quote: "Technology has been a tool through which humans create distance from natural cycles and design their own time experiences. This means we can critically intervene in the functioning of the technology and develop alternative time practices and experiences." In this section, he goes on to describe how we can program the computer to follow the sundial time in order to reconnect with the more ancient, natural experience of time. In the beginning of the text, he desribes how our experience of time as a natural life cycle has changed by the interference of technology and social conditions. I was then reminded of my own computer, and of the "night light" screen function that it has. During the day, the screen light is set to normal, and it emits the regular blue lights that it usually does. But when the sun goes down in real life, the computer starts to automatically emit more red-light, and as we know that blue light represses melatonin and makes it harder to fall asleep, the emission of red light helps my body prep for sleep just like the disapperance of the sun does, thus making me follow the natural human time-schedule. 

What I have changed in my code is a simple as the background colors. When the sun is away from the earth, it is night, and therefore the screen becomes red to supress the amount of blue lights coming out. When the sun is over the earth, it is daytime, and the screen becomes blue, simulating the blue light emissions from both the sun and the screen that you are looking at. I see this as a unification of our natural selves and technology. Before the industrial times, the body would regulate its temperature and sleep hormones through the light that came from the sun, and the lack of it as well, thereby knowing when it is time to sleep and when it is time to wake up. The invention of technology, as Lammerant touches upon as well, has disrupted this natural cycle, but by programming the computer to follow the sun, it is then possible to take that power back again.


**How would you demonstrate aesthetic programming in your work?**

I have demonstrated aesthetic progrmamming by making a sketch that is not purely functional, but has more of a symbolic value, which is the essense of the subject for me. Aesthetic programming is about making conscious creative choices for your program, that not only looks aesthetically pleasing but also serves an underlying purpose about an issue or topic. For me, I chose to reflect upon Lammerant and his considerations about the human experience of time, and of how the computer regulates this experience in a biological sense.


**What does it mean by programming as a practice, or even as a method for design?**

I think what I am learning is that programming does not have to be purely practical, whether it be for creating new software systems or making websites, but that it can be more than that. It works a a method for design in the way that it enables the designer to mix practicality and criticality into one practise. 


**What is the relation between programming and digital culture?**

I think that there are two sides to programming and its impact on digital culture: on one hand, the software programs that are created to work as tools for the designers have an impact on what they make, thus shaping our digital culture. On the other hand, and perhaps as a result of the first, programming has become more and more available for all kinds of people, and by being more inclusive it has led to more diversity in programming and design, shaping the cultural products of the digital world.


**Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?**

For me, critical making is not only about creating something that takes a critical stance on societal/cultural/political issues, but it is also important to be critical of what you are making and the process of creation. In this miniX, I believe I have utilized a part the ideological foundation of what it means to be a critical maker, by taking a look at my old miniX3 and using the feedback that I got from others to revise it and try to see it from another persepctive.

**Rerefences**

Lammerant, Hans: _How humans and machines negoiate the experience of time_. 2017. Association for Art and Media.
