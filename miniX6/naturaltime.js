var x = 0;
var speed = 3;

function setup(){
  createCanvas(windowWidth, windowHeight);
  }

function draw (){
    background(255,0,0,3); //daylight - blue light
    drawEarth();
    drawSun();
}

function drawEarth(){
  noStroke();
  textSize(100);
  text('🌍', 1200, 395);

  }

function drawSun(){
noStroke();
fill(255,150,0);
ellipse(x,365,900,900);

if ((x > 800) && (x < 2000)){
  background(0,0,255,30); //nighttime - red light
}else if (x > 1000){
  x = -2;

}
x = x + speed;
}
