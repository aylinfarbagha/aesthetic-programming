**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

Generally speaking we did not have much trouble making the flowchart for our ideas, but we spent more time on trying to figure out and formulate our ideas, in order to have a specified starting point. Once we had most of it figured out, making the flowcharts went pretty fast. 

The difficult part was to make something that would make sense for each member in the group in terms of the conceptualization of our ideas. We have included the essential overall features in our charts, but there are of course a lot of variables that have not been accounted for, that each member has a vague idea about. These are mostly the aesthetic values of our project ideas, whose symbolic meanings have yet to be determined once we choose an idea and dig deeper into its conceptual significance.

It should also be mentioned that we made our flowcharts on a Miro board, which enabled us all to make and edit them at the same time. This made it a lot easier in terms of communication and worked as a very good solution to making all of this online.

We observed that when the flowchart was made in the group prior to the code for our next project, the focus centered on defining the logical processes more than how the code would “think” and work. Thereby we had more focus on simple communication of the idea. This was opposed to some of our individual flowcharts, which due to the fact that we had a code prewritten, more easily had a focus on the algorithmic complexity, and the use of language in the flow charts also resembled how language was used on a code level. 


**What are the technical challenges facing the two ideas and how are you going to address these?**
 							
We tried to bridge the gap in our skill levels by discussing each idea thoroughly, not just conceptually but also technically. Both ideas that were chosen have a low fidelity solution connected to it, ensuring that a final product can be achieved without members getting left out of the programming process. This is very important, as we want everyone to be able to talk about and explain the technical aspects of the code that we are going to make, as we all have our strengths and weaknesses in this course.
								
**In which ways are the individual and the group flowcharts you produced useful?**		
				
The group flowcharts act as blueprints for the final sketch. Through dialogue and knowledge from the course all other details can be abstracted by each respective member. 

**Aylin**

Personally I think that the communication aspect can work really well when we use apps like Miro board, but when we have to go in-depth and explain conceptually difficult ideas, it is best to work together physically. There are many technical challenges for me as I an not very good at coding, but I feel like it's going to work out as I am with very capable people. The important thing here is just to stay engaged when difficult syntaxes are made, so that I can understand them completely when I have to talk about them during exam. My own individual flowchart was good to make as it helped me understand how to formulate a project that I had already done. The group flowcharts were a new way of working, something we hadn't done before, where the flowchart came as a guide to the idea that we want to produce. This has given me a good idea of what we are going to make, and it makes the whole final project seem a bit more manageable. 
