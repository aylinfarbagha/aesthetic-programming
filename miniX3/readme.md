Link to project: https://aylinfarbagha.gitlab.io/aesthetic-programming/miniX3/

For this sketch I primarirly followed some of The Coding Train's videos such as 'The Bouncing Ball' to get my sun moving. I then learned about conditional statements to make the sketch reset, otherwise it wouldn't have been a throbber.

My sketch is supposed to represent the sun moving towards the Earth resulting in an ultimate collision. Everything turns red as our planet burns up, and it all starts over.

My code is very simple. I made two variables to set the values for x and speed. I then created my Earth by copying the Earth emoji from emojipedia.org and afterwards just made an orange ellipse for the sun. 

I created the conditional statement:
if ((x > 800) && (x < 1000)){
  background(255,0,0);
  fill(255,0,0);
  rect(1200,395);
  }
x = x + speed;
}
This made sure that when the sun was between the x coordinates 800-1000, the screen would turn red.

I then said:
}else if (x > 1000){
  x = -2;
To make sure that when x exceeded 1000, it would turn to the x coordinate -2 and thus start all over again.

The throbber isn't very pretty, as you can see the sun turning to the -2 x coordinate when it exceeds 1000, instead of this just happening without being detected. This must have something to do with the function redrawal time and the sun going to -2 overlapping with each other, but I don't know.

I wanted to explore the idea of throbbers in a different way than you usually see on websites, apps and various softwares. A throbber is essentially something that is in a loop, repeating its motions over and over again, which means it can look like anything as long as it has this function. I wanted to rethink what a throbber ought to look like vs. what it could look like, which in my project is something more graphic.

The time-related syntaxes in my project present themselves as variables and are measured by the coordinates, i.e. x and var = speed. I have also used the x coordinate to manipulate time, as I wanted the red background to appear for a few seconds (for dramatic effect) before the sketch started all over again, which I did by giving x the time to move from 800-1000 before adding the 'else if' function.
