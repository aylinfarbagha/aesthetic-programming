var x = 0;
var speed = 2;

function setup(){
  createCanvas(windowWidth, windowHeight);
  }

function draw (){
    background(0);
    drawEarth();
    drawSun();
}

function drawEarth(){
  noStroke();
  textSize(100);
  text('🌍', 1200, 395);

  }

function drawSun(){
noStroke();
fill(255,150,0);
ellipse(x,365,900,900);

if ((x > 800) && (x < 1000)){
  background(255,0,0);
  fill(255,0,0);
  rect(1200,395);
}else if (x > 1000){
  x = -2;

}
x = x + speed;
}
