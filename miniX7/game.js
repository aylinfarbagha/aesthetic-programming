let picklerickSize = {
  w:120,
  h:130
};
let picklerick;
let rats;
let picklePosY;
let picklePosX;
let mini_height;
let min_rat = 20;  //min rats on the screen
let rat = [];
let score =0, lose = 0;
let keyColor = 45;

function preload(){
  picklerick = loadImage("picklerick.png");
  rats = loadImage("rat.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  picklerickPosY = height/2;
  picklerickPosX = width/2;
  mini_height = height/2;
}
function draw() {
  background(243);
  fill(keyColor, 255);
  displayScore();
  checkRatNum(); //available tofu
  showRat();
  image(picklerick, mouseX-45, mouseY-45,90,90);
    //picklerickPosX, picklerickPosY, 90, 90);

  checkEating(); //scoring
  checkResult();

}

function checkRatNum() { //telling the computer to make new rats - conditional statement
  if (rat.length < min_rat) {
    rat.push(new Rat()); //push is a generative function
  }
}

function showRat(){ //making sure all the objects are displayed on the screen through loop
  for (let i = 0; i <rat.length; i++) {
    rat[i].move(); //telling rats to move
    rat[i].show(); //making rats show
  }
}

function checkEating() { //if rat position moves beyond the screen, it must be deleted
  //calculate the distance between each rat
  for (let i = 0; i < rat.length; i++) {
    let d = int(
      dist(mouseX,mouseY,rat[i].pos.x, rat[i].pos.y)); //missing something with this - need to know how to make picklerick eat the rats
    if (d < picklerickSize.w/2.5) { //close enough as if eating the tofu
      score++;
      rat.splice(i,1);
    }else if (rat[i].pos.x < 3) { //pacman missed the tofu
      lose++;
      rat.splice(i,1); //splice deletes, i = which one to delete in array, 1 = how many forward in the array to delete
    }
  }
}

function displayScore() {
    fill(keyColor, 160);
    textSize(20);
    text('You have eaten '+ score + " rat(s)", 10, height/1.4);
    text('You have wasted ' + lose + " rat(s)", 10, height/1.4+20);
    fill(keyColor,255);
    text('Move Pickle Rick around with your mouse to eat the rats',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > score && lose > 5) {
    fill(keyColor, 255);
    textSize(26);
    text("TOO MANY RATS ... PICKLE RICK IS NO MORE", width/3, height/1.4);
    noLoop();
  }
}

// function keyPressed() {
//   if (keyCode === UP_ARROW) {
//     picklerickPosY-=50;
//   } else if (keyCode === DOWN_ARROW) {
//     picklerickPosY+=50;
//   } else if (keyCode === LEFT_ARROW) {
//       picklerickPosX-=50;
//   } else if (keyCode === RIGHT_ARROW) {
//       picklerickPosX+=50;
//   }
// }
