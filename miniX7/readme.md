Link to runme: https://aylinfarbagha.gitlab.io/aesthetic-programming/miniX7/

My game is based on Rick & Morty, specifically the episode where Rick becomes Pickle Rick and has to fight the rats in the sewer and collect their body parts in order to be a functioning pickle. The user controls Pickle Rick by using the mouse, and "eats" the rats as they come through. I have created the rats as a class object, and given them random speeds at which to appear as well as a minimum limit of rats to appear on the screen at the same time, which is 20. As is very evident, my code is inspired (a lot) from Winnie's Tofu game.

The object of this program is the rat, with different properties such as speed, size and location. The location is defined through a vector, so that I could make the rats appear on a specific part of the canvas, and not go over the text. The speed and size is set at random but through specific confinements, which I used as a means of adapting and setting the difficulty level of the game. If you miss more rats than what you have eaten, you loose, and for this logic I needed it to not be too easy, thus the high speed and many rats.

Abstraction in itself is very useful when trying to use simple data to define objects when making easy games. But as the games become more real-like, and perhaps try to mimick the real world, real people, and real interactions, as we see in RPG, open-world games, the process of abstraction and OOP can become more complicated.  

"a computational object is a partial object: the properties and methods that define it are a necessarily selective and creative abstraction of particular capacities from the thing it models, and which specify the ways that it can be interacted with." (Fuller & Goffey, 2017, 6).

This quote is very relevant when looking at creating a character or an object in a RPG, because the abstracting of real life, which ultimately occurs, is often based on the creator's personal world views and biases. This can be a good thing when something specific has to be made, which they happen to have a lot of knowledge about, but also a bad thing, when an ethnicity, sexuality, gender or other types of identities has to be represented in a character, and the creator lacks understanding of how best to represent their attributes and possible interactions.

