let bgColor;
let img;

function setup(){
img = loadImage ('kat hvid.png');

createCanvas(windowWidth,windowHeight);
bgColor = (0,0,0);
}

function draw(){
background (bgColor);
textSize(64);
textAlign(CENTER)
text('😻😽😹😿😾🙀', width/2, height/2)
}

function mousePressed () {
  bgColor = (255,255,255);
}
