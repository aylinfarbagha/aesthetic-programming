//arrays
let sign = ["Aries","Taurus","Gemini","Cancer","Leo","Virgo","Libra","Scorpio","Saggitarius","Capricorn","Aquarius","Picses"];
let elements = ["Fire","Water","Air","Earth"];
let date = ["MAR21 - APR19", "APR20 - MAY20", "MAY21 - JUN20", "JUN21 - JUL22", "JUL23 - AUG22", "AUG23 - SEP22", "SEP23 - OCT22", "OCT23 - NOV21", "NOV22 - DEC21", "DEC22 - JAN19", "JAN20 - FEB18", "FEB19 - MAR20"];
let planet = ["Sun","Moon","Mercury","Venus","Mars","Jupiter","Saturn","Uranus","Neptune","Pluto"];
let house = ["1st House", "2nd House", "3rd House", "4th House", "5th House", "6th House", "7th House", "8th House", "9th House", "10th House", "11th House", "12th House"];
let personality = ["Bright", "Strong", "Witty", "Compassionate", "Proud", "Kind", "Peaceful", "Bold", "Honest", "Clever", "Sincere", "Creative"];
let x = 0;
let y = 0;

function setup(){
frameRate(2);
createCanvas (windowWidth,windowHeight);
background(255);
}

function draw(){
background(255);
fill(0);
textSize(20);
text(random(sign), 100, 200);
text(random(elements), 400,200);
text(random(date), 600,200);
text(random(planet), 900,200);
text(random(house), 1100,200);
text(random(personality), 1350,200);

//sun and moon on top
if (random(1)<0.5){
  noStroke();
  fill(255,200,50);
  ellipse(x+50,y+50,50,50);
}else{
  noStroke();
  fill(0,0,255,20);
  ellipse(x+50,y+50,80,80);
}
x = x + 50

if (random(1)<0.5){
  ellipse(x+1000,y+100,50,50);
}else{
  ellipse(x+1000,y+100,80,80);
   }

//start over
if (x>width){
  x = 0;
  y = y + 20
}

//background color
if (random(1) > 0.5){
background(300, 200,100,20);
}else if (random(1)<0.5){
background(255,0,0,20);
}

//ellipse loop
for(var i=0;i<1;i++){
  noStroke();
  fill(255,0,0,50);
  ellipse(random(windowWidth),random(windowHeight),50,50);
  fill(0,0,255,50);
  ellipse(random(windowWidth),random(windowHeight),70,70);
  fill(0,0,255,90);
  ellipse(random(windowWidth),random(windowHeight),50,50);
  fill(0,255,0,50);
  ellipse(random(windowWidth),random(windowHeight),70,70);
  fill(0,255,0,90);
  ellipse(random(windowWidth),random(windowHeight),50,50);
  fill(0,255,0,90);
  ellipse(random(windowWidth),random(windowHeight),70,70);
   }
}
