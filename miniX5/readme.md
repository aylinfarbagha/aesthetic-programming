Link: https://aylinfarbagha.gitlab.io/aesthetic-programming/miniX5/

I wanted to make a program that functioned as a sort of funny commentary to the field of astrology, which is why the random() function appears a lot. I have multiple rules. Some of them are:
1. The two ellipes at the top, which are supposed to represent the sun and moon passing, must move along the x-axis, reset and move down when they've reached the end of the canvas width.
2. The background color will switch colors depending on if the random value is above or belove 0.5, giving it a 50% change of each color (although somehow there are three colors which I couldn't quite understand why).
3. All the other ellipses, which are supposed to represent planets, must appear at random inside the windowWidth and windowHeight in a loop.
4. The words you see must appear at random.

My role of rules in my sketch is to always have everything changing and moving using the random value. In astrology, everything is determined by specificities: date of birth, coordinates of the planets, location, which then determines sign, elements, houses and personality traits. It is all interconnected and yet there seems to be a sense of randomness to it?

In terms of autogenerativity my sketch has influence from both the random, which makes the overall look unpredicatble, but as the creator and the one who makes the rules, I have still retained a lot of autonomy in how the overall sketch should look. I have in some sense controlled the random as well, by determining the different values that set the speed of the framerate, speed of the movement of ellipses, the different sizes to shift between, and which words to choose between in the arrays. 
